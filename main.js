const API = 'https://jsonplaceholder.typicode.com';
const controller = action => fetch(action).then(response => response.json().catch(err => console.log(err)));

const form = document.querySelector('.form');
const post = document.querySelector('.post');

form.addEventListener('submit', e => {
    e.preventDefault();

    const searchValue = document.querySelector('.input').value;

    if (searchValue) {
        post.classList.remove('d-none');
        controller(`${API}/posts/${searchValue}`)
            .then(data => {
                const div = document.createElement('div');
                const title = document.createElement('h3');
                const pBody = document.createElement('p');
                const btn = document.createElement('button');

                div.classList.add('main-post');

                btn.addEventListener('click', () => {
                    controller(`${API}/comments?postId=${searchValue}`)
                        .then(data => {
                            data.forEach(el => {
                                const div = document.createElement('div');
                                const body = document.createElement('p');
                                const name = document.createElement('h3');

                                div.classList.add('post-comments');

                                body.innerText = el.body;
                                name.innerText = el.name;
                                btn.classList.add('d-none');

                                div.append(name);
                                div.append(body);

                                post.append(div);
                            });
                        })
                        .catch(err => console.log(err));
                });

                btn.innerHTML = 'Comments';
                title.innerText = data.title;
                pBody.innerText = data.body;

                div.append(title);
                div.append(pBody);
                div.append(btn);

                post.append(div);
            })
            .catch(err => {
                post.classList.add('d-none');
                console.log(err);
            });
    }
});
